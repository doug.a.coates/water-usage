# Usage:

``` bash
python main.py image_file coords data scaling output_dir
```
where: 
 - `image_file`:    path to image to draw on
 - `coords`:    csv file containing coordinates of the taps, each row a different tap, columns are (x,y) coordiantes (measured from top left hand corner of the image)
 - `data`:    csv file with usage data, rows are hours/months/days and columns are different taps with the exception of the first column which is just the label used for each row, i.e. 9:00am/January/Wednesday (no spaces in labels)


# Requirements

 - Python 3
 - matplotlib

