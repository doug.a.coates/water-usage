import os
import sys
import csv
import matplotlib.pyplot as plt

def sanity_checks():
    '''Check that data is well formated and all files exist'''
    global IMAGE_FILE
    global COORDINATES
    global USAGE_DATA
    # check image file exists
    if not os.path.isfile(IMAGE_FILE):
        print('Image file {} does not exist'.format(IMAGE_FILE))
        exit(1)
    no_taps = len(COORDINATES)
    if no_taps == 0:
        print('No taps!')
        exit(1)
    if len(USAGE_DATA) == 0:
        print('Empty data')
        exit(1)
    if len(USAGE_DATA[0]) <= 1:
        print('No data')
        exit(1)
    row_length = len(USAGE_DATA[0])
    for row in USAGE_DATA:
        if len(row) != row_length:
            print('Rows in input data have different lengths')
            exit(1)


def make_plot(image_file, coords, data, scaling, label, output_dir):

    img = plt.imread(image_file)
    fig, axis = plt.subplots()
    axis.imshow(img)

    for data_pt, index  in zip(data, range(len(data))):
        radius = data_pt * scaling
        coord = coords[index]
        circle = plt.Circle(coord, radius, color='#87CEEB', alpha=0.3)
        axis.add_artist(circle)

    fig.suptitle(label)
    filename = os.path.join(output_dir, 'image_{}.png'.format(label))
    fig.savefig(filename)


def read_coords(coords_file):
    if not os.path.isfile(coords_file):
        print('Cannot find coordinate file {}'.foramt(coords_file))

    print('Getting coordinates for {}'.format(coords_file))
    coords = []
    with open(coords_file, 'r') as f:
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader:
            if len(row) < 2:
                print('Not enough columns in coordinate data')
                exit(1)
            coords.append((int(row[0]), int(row[1])))
    return coords

def read_data(data_file):
    if not os.path.isfile(data_file):
        print('Cannot find data file {}'.format(data_file))

    data = []
    print('Getting data from {}'.format(data_file))
    with open(data_file, 'r') as f:
        csv_reader = csv.reader(f)
        for row in csv_reader:
            r = []
            index = 0
            for i in row:
                if index != 0:
                    i = int(i)
                r.append(i)
                index += 1
            data.append(r) 
    return data

def main(image_file, coords, data, scaling, output_dir):
    '''For each column in input data plot the water usage and save as an image'''

    # make an image for each time point 
    for row in data:
        label = row[0]
        data = row[1:]
        print('Producing a image with for {} with data'.format(label))
        print(data)
        make_plot(image_file, coords, data, scaling, label, output_dir)

def print_usage():
    print('''
Usage:
------
python main.py image_file coords data scaling output_dir

Requirements:
-------------
 - Python 3
 - matplotlib

Arguments:
----------
image_file:    path to image to draw on

coords:    csv file containing coordinates of the taps, each row a different tap, columns are (x,y) coordiantes (measured from top left hand corner of the image)

data:    csv file with usage data, rows are hours/months/days and columns are different taps with the exception of the first column which is just the label used for each row, i.e. 9:00am/January/Wednesday (no spaces in label please!)''')

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print_usage()
        exit(1)

    image_file, coords_file, data_file, scaling, output_dir = sys.argv[1:]
    scaling = float(scaling)
    coords = read_coords(coords_file)
    data = read_data(data_file)
    print(data)
    # sanity_checks(image_file,coords,data, output_dir)
    main(image_file, coords, data, scaling, output_dir)
